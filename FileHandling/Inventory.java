package FileHandling;



import java.util.Arrays;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author bas200188
 */
public class Inventory {
    private int pro_id;
    private String productName;
    private int proQuantity;
    private double price;
    int k=1;
    public Inventory(int pro_id, String productName, int proQuantity, double price) {
        this.pro_id = pro_id;
        this.productName = productName;
        this.proQuantity = proQuantity;
        this.price = price;
    }
    public Inventory() {
    }
    public int getPro_id() {
        return pro_id;
    }
    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public int getProQuantity() {
        return proQuantity;
    }
    public void setProQuantity(int proQuantity) {
        this.proQuantity = proQuantity;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    @Override
    public String toString() {
        return "product ID:" +pro_id + "\n" + "product Name:" + productName + "\n" + "product quantity:" + proQuantity+"\n"+"product price ="+price;
    }
    }