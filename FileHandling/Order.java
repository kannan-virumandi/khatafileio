package FileHandling;



/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author bas200188
 */

import java.time.LocalDate;
public class Order{
    Customer d = new Customer();
    private int orderId;
    private int cusId;
    private LocalDate date;
    private String deliveredAddress;
    private double totalprice;
    private double balancePayment;
    public Order(int orderId, int cusId, LocalDate date, String deliveredAddress, double totalprice,double balancePayment) {
        this.orderId = orderId;
        this.cusId = cusId;
        this.date = date;
        this.deliveredAddress = deliveredAddress;
        this.totalprice = totalprice;
        this.balancePayment=balancePayment;
    }
    public Order() {
        super();
    }
    public double getBalancePayment() {
        return balancePayment;
    }
    public void setBalancePayment(double balancePayment) {
        this.balancePayment = balancePayment;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public void setCusId(int cusId) {
        this.cusId = cusId;
    }
    public void setDeliveredAddress(String deliveredAddress) {
        this.deliveredAddress = deliveredAddress;
    }
    public int getOrderId() {
        return orderId;
    }
    public int getCusId() {
        return cusId;
    }
    public String getDeliveredAddress() {
        return deliveredAddress;
    }
    @Override
    public String toString() {
        return  "orderId :  "  + orderId+"\n" + "cutomer ID  :  " + cusId  + "\n" +"Date  :   "+ date+"\n"+ "deliveredAdress :   " + deliveredAddress+"\n"+"total price  =  "+totalprice+"\n"+"Balancepayment   ="+balancePayment;
    }
    public double getTotalprice() {
        return totalprice;
    }
    public void setTotalprice(double totalprice) {
        this.totalprice = totalprice;
    }
    public static void main(String[] args) {
        FileRead re = new FileRead();
        re.customerDataReader();
    }
}