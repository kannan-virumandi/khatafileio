package FileHandling;


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author bas200188
 */

 import java.util.Scanner;
public class Customer{
    Scanner s = new Scanner(System.in);
    private int cusId;
    private String customerName;
    private  long phoneNumber;
    public Customer(int cusId, String customerName, long phoneNumber) {
        this.cusId = cusId;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
    }
    public Customer() {
    }
    public int getCusId() {
        return cusId;
    }
    public void setCusId(int cusId) {
        this.cusId = cusId;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public long getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
      @Override
    public String toString() {
        return "cutomer ID:" + cusId + "\n" + "Customer Name:" + customerName + "\n" + "phonemumber:" + phoneNumber;
    }
}