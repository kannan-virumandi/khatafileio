package FileHandling;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author bas200188
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.*;

public class KhataFileIo {

    static BufferedReader s1 = new BufferedReader(new InputStreamReader(System.in));
    static Customer[] cus = new Customer[0];
    static LineOrder[] l = new LineOrder[0];
    static Inventory[] d = new Inventory[0];
    static Order or[] = new Order[0];

    public static void main(String[] args) throws Exception {

        FileWrite rw = new FileWrite();
        FileRead re = new FileRead();
        re.customerDataReader();
        re.InvemtoryReader();
        re.orderReader();
        re.lineOrderReader();
        boolean t = true;
        KhataFileIo kh = new KhataFileIo();

        try {

            do {
                System.out.println("           Grow your business with us");
                System.out.println("           /<>/<>/<>/<>/<>/<>/<>/<>/<>/");
                System.out.println("           ==========================");
                System.out.println("           ||      1.customer      ||");
                System.out.println("           ||      2.inventory     ||");
                System.out.println("           ||      3.Statics       ||");
                System.out.println("           ||      4.Exit          ||");
                System.out.println("           ==========================");
                int m = 0;
                try {
                    System.out.println("Enter the menu option");
                    m = Integer.parseInt(s1.readLine());
                } catch (Exception e) {
                    System.out.println(e);
                }
                switch (m) {
                    case 1: {
                        boolean cu = true;
                        do {
                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^");
                            System.out.println("|1.customer order     |");
                            System.out.println("|2.Add customer       |");
                            System.out.println("|3.View Customer      |");
                            System.out.println("|4,update Customer    |");
                            System.out.println("|5.Add order          |");
                            System.out.println("|6.payment            |");
                            System.out.println("|7.Exit               |");
                            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^");
                            int c = 0;
                            try {
                                System.out.println("Enter the Customer menu option");
                                c = Integer.parseInt(s1.readLine());
                            } catch (Exception e) {
                                System.err.println(e);
                            }
                            switch (c) {
                                case 1: {
                                    try {
                                        System.out.println("    ************* customer placed orders Detail************   ");
                                        for (int i = 0; i < or.length; i++) {
                                            System.out.println(or[i]);
                                            System.out.println("");
                                            for (int j = 0; j < l.length; j++) {
                                                if (or[i].getOrderId() == l[j].getOrderId()) {
                                                    System.out.println(l[j]);
                                                    System.out.println(" ");
                                                }
                                            }
                                            System.out.println(" <><><><><><><><><><><><><><><><><><><><><><><><><><><>");
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 2: {

                                    try {
                                        cus = Arrays.copyOf(cus, cus.length + 1);
                                        System.out.println("enter your id");
                                        int Id = Integer.parseInt(s1.readLine());
                                        System.out.println("Enter the Customer name");
                                        String name = s1.readLine();
                                        System.out.println("Enter the phone number");
                                        long phonenumber = Long.parseLong(s1.readLine());
                                        cus[cus.length - 1] = new Customer(Id, name, phonenumber);
                                        System.out.println("customer details add sucessfully");
                                        System.out.println("");

                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    } catch (Exception e) {
                                        System.err.println(e);
                                    }
                                    break;
                                }
                                case 3: {
                                    try {
                                        System.out.println("1.view all");
                                        System.out.println("2.view particular customer");
                                        System.out.println("Enter the option");
                                        int v = Integer.parseInt(s1.readLine());
                                        switch (v) {
                                            case 1: {
                                                for (int i = 0; i < cus.length; i++) {
                                                    System.out.println("    <>/<>/<>/<>/Customer Detail<>/<>/<>/<>/   ");
                                                    System.out.println(cus[i]);
                                                    System.out.println("");
                                                }
                                                break;
                                            }
                                            case 2: {
                                                System.out.println("Enter the customer id");
                                                int id = Integer.parseInt(s1.readLine());
                                                System.out.println("    <>/<>/<>/<>/<>/Customer Detail<>/<>/<>/<>/<>/   ");
                                                for (int i = 0; i < cus.length; i++) {
                                                    if (cus[i].getCusId() == id) {
                                                        System.out.println(cus[i]);
                                                        System.out.println("");
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    } catch (Exception e) {
                                        System.err.println(e);
                                    }
                                    System.out.println("");
                                    break;
                                }
                                case 4: {

                                    try {
                                        System.out.println("Enter the customer Id, which customer detail you want to change");
                                        int y = Integer.parseInt(s1.readLine());

                                        for (int i = 0; i < cus.length; i++) {
                                            if (cus[i].getCusId() == y) {
                                                System.out.println("1.Customer name");
                                                System.out.println("2.PhoneNumber");
                                                System.out.println("Enter the feild to update customer");
                                                int option = Integer.parseInt(s1.readLine());
                                                switch (option) {
                                                    case 1:
                                                        System.out.println("Enter the updated Customer Name");
                                                        String k = s1.readLine();
                                                        cus[i].setCustomerName(k);
                                                        break;
                                                    case 2:
                                                        System.out.println("Enter the updated Customer Phone Number");

                                                        long o = Long.parseLong(s1.readLine());
                                                        cus[i].setPhoneNumber(o);
                                                    default:
                                                        System.err.println("Enter the correct option");
                                                        break;
                                                }
                                            }

                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    } catch (Exception e) {
                                        System.err.println(e);
                                    }
                                    System.out.println("");
                                    break;
                                }
                                case 5: {
                                    try {
                                        System.out.println("enter the Customer Id to add orders");
                                        int cusid = Integer.parseInt(s1.readLine());
                                        for (int i = 0; i < cus.length; i++) {
                                            if (cus[i].getCusId() == cusid) {
                                                double total = kh.addItems();
                                                or = Arrays.copyOf(or, or.length + 1);
                                                System.out.println("enter the cusid already given");
                                                int cusId = cus[i].getCusId();
                                                int orderId = l[l.length - 1].getOrderId();
                                                LocalDate date = LocalDate.now();
                                                System.out.println("Enter the delivary addres");
                                                String delAdd = s1.readLine();

                                                or[or.length - 1] = new Order(orderId, cusId, date, delAdd, total, total);
                                                System.out.println("Order placed");
                                                break;
                                            }

                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }

                                    break;

                                }

                                case 6: {
                                    try {
                                        System.out.println("Enter customer ID for payment");
                                        int cusid = Integer.parseInt(s1.readLine());
                                        for (int i = 0; i < or.length; i++) {
                                            if (or[i].getCusId() == cusid) {
                                                System.out.println("total price balance" + or[i].getTotalprice());
                                                double payed = or[i].getTotalprice() - or[i].getBalancePayment();
                                                System.out.println("Already payed amount:" + payed);
                                                System.out.println("Enter the Amount pay by customer");
                                                double pay = Double.parseDouble(s1.readLine());
                                                double balance = or[i].getBalancePayment() - pay;
                                                or[i].setBalancePayment(balance);
                                                System.out.println("customer payable balance amount :" + or[i].getBalancePayment());
                                            }
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 7: {
                                    cu = false;
                                    break;
                                }
                                default: {
                                    System.err.println("Enter the correct menu option");
                                }
                            }
                        } while (cu);
                        break;
                    }

                    case 2: {
                        boolean in = true;
                        do {
                            System.out.println("|-------------------------|");
                            System.out.println("|1.View product           |");
                            System.out.println("|2.Add new product        |");
                            System.out.println("|3.update product         |");
                            System.out.println("|4.Exit                   |");
                            System.out.println("|-------------------------|");
                            int pr = 0;
                            try {
                                System.out.println("Enter the menu Inventory option");
                                pr = Integer.parseInt(s1.readLine());
                            } catch (Exception e) {
                                System.err.println(e);
                            }
                            switch (pr) {
                                case 1: {
                                    int v = 0;

                                    System.out.println("1.view all");
                                    System.out.println("2.view particular customer");
                                    while (true) {
                                        try {
                                            System.out.println("Enter the option");
                                            v = Integer.parseInt(s1.readLine());
                                            break;
                                        } catch (NumberFormatException | InputMismatchException e) {
                                            System.err.println("Enter the correct input" + e);
                                        }
                                    }
                                    switch (v) {
                                        case 1: {
                                            try {
                                                System.out.println("    <>/<>/<>/<>/<>/Customer Detail<>/<>/<>/<>/<>/   ");
                                                for (int i = 0; i < d.length; i++) {
                                                    System.out.println(d[i]);
                                                    System.out.println("");
                                                }
                                            } catch (NullPointerException npe) {
                                                System.out.println(npe);
                                            } catch (ArrayIndexOutOfBoundsException aiob) {
                                                System.out.println(aiob);
                                            }
                                            break;
                                        }
                                        case 2: {
                                            try {

                                                System.out.println("Enter the product id");
                                                int id = Integer.parseInt(s1.readLine());
                                                for (int i = 0; i < d.length; i++) {
                                                    if (d[i].getPro_id() == id) {
                                                        System.out.println("    <>/<>/<>/<>/<>/product Detail<>/<>/<>/<>/<>/   ");
                                                        System.out.println(d[i]);
                                                        System.out.println("");
                                                    }
                                                }
                                            } catch (NullPointerException npe) {
                                                System.out.println(npe);
                                            } catch (ArrayIndexOutOfBoundsException aiob) {
                                                System.out.println(aiob);
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case 2: {
                                    try {
                                        d = Arrays.copyOf(d, d.length + 1);
                                        System.out.println("your product id is");
                                        int proid = Integer.parseInt(s1.readLine());
                                        System.out.println("Enter the Product Name");
                                        String name = s1.readLine();
                                        System.out.println("Enter the product Quantity");
                                        int proquan = Integer.parseInt(s1.readLine());
                                        System.out.println("Enter the Product Price");
                                        double price = Integer.parseInt(s1.readLine());
                                        d[d.length - 1] = new Inventory(proid, name, proquan, price);
                                        System.out.println("product added sucessfully");
                                        System.out.println("");
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    } catch (Exception e) {
                                        System.out.println(e);
                                    }
                                    break;
                                }
                                case 3: {
                                    try {
                                        System.out.println("Enter the product id");
                                        int proid = Integer.parseInt(s1.readLine());
                                        for (int i = 0; i < d.length; i++) {
                                            if (d[i].getPro_id() == proid) {
                                                System.out.println("1.product name");
                                                System.out.println("2,product quantity");
                                                System.out.println("Enter the feild to update");
                                                int option = Integer.parseInt(s1.readLine());
                                                switch (option) {
                                                    case 1:
                                                        System.out.println("Enter the updated product name");
                                                        String f = s1.readLine();
                                                        d[i].setProductName(f);
                                                        break;
                                                    case 2:
                                                        System.out.println("Enter the updated product quantity");
                                                        int h = Integer.parseInt(s1.readLine());
                                                        d[i].setProQuantity(h);
                                                        break;
                                                    default:
                                                        System.out.println("Enter the correct option");
                                                        break;
                                                }
                                            }
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 4: {
                                    in = false;
                                    break;
                                }
                                default: {
                                    System.err.println("Enter the correct menu option");
                                }
                            }
                        } while (in);
                        break;
                    }
                    case 3: {
                        boolean sts = true;
                        do {
                            System.out.println("1.Maximun selling amount order detail (week or month)");
                            System.out.println("2.Minimum selling amount order detail (week or month)");
                            System.out.println("3.Based on date customer order");
                            System.out.println("4.Most sell product");
                            System.out.println("5.Most profit product");
                            System.out.println("6.Exit");
                            int st = 0;
                            try {
                                System.out.println("Enter the statics menu option");
                                st = Integer.parseInt(s1.readLine());
                            } catch (Exception e) {
                                System.err.println(e);
                            }
                            switch (st) {
                                case 1: {
                                    try {
                                        System.out.println("Enter week or month  start  date(yyyy-mm-dd)");
                                        String sr = s1.readLine();
                                        System.out.println("Enter week or month end date(yyyy-mm-dd)");
                                        String ed = s1.readLine();
                                        LocalDate start = LocalDate.parse(sr);
                                        LocalDate end = LocalDate.parse(ed);
                                        double maxsell = 0;
                                        int ordid = 0;
                                        while (!start.isAfter(end)) {
                                            for (int j = 0; j < or.length; j++) {
                                                if (or[j].getDate().equals(start)) {
                                                    if (maxsell < or[j].getTotalprice()) {
                                                        maxsell = or[j].getTotalprice();
                                                        ordid = or[j].getOrderId();
                                                    }
                                                }
                                            }
                                            start = start.plusDays(1);
                                        }
                                        System.out.println("Maximum selling amount order");
                                        for (int i = 0; i < or.length; i++) {
                                            if (or[i].getOrderId() == ordid) {
                                                System.out.println(or[i]);
                                            }
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 2: {
                                    try {
                                        System.out.println("Enter week or month  start  date(yyyy-mm-dd)");
                                        String sr = s1.readLine();
                                        System.out.println("Enter week or month end date(yyyy-mm-dd)");
                                        String ed = s1.readLine();
                                        LocalDate start = LocalDate.parse(sr);
                                        LocalDate end = LocalDate.parse(ed);
                                        double minsell = or[0].getTotalprice();
                                        int ordid = or[0].getOrderId();
                                        while (!start.isAfter(end)) {
                                            for (int j = 0; j < or.length; j++) {
                                                if (or[j].getDate().equals(start)) {
                                                    if (minsell >= or[j].getTotalprice()) {
                                                        minsell = or[j].getTotalprice();
                                                        ordid = or[j].getOrderId();
                                                    }
                                                }
                                            }
                                            start = start.plusDays(1);
                                        }
                                        System.out.println("Minimum selling amount order");
                                        for (int i = 0; i < or.length; i++) {
                                            if (or[i].getOrderId() == ordid) {
                                                System.out.println(or[i]);
                                            }
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 3: {
                                    try {
                                        System.out.println("Enter week or month start  date(yyyy-mm-dd)");
                                        String sr = s1.readLine();
                                        System.out.println("Enter week or month end date(yyyy-mm-dd)");
                                        String ed = s1.readLine();
                                        LocalDate start = LocalDate.parse(sr);
                                        LocalDate end = LocalDate.parse(ed);
                                        while (!start.isAfter(end)) {
                                            for (int j = 0; j < or.length; j++) {
                                                if (or[j].getDate().equals(start)) {
                                                    System.out.println(or[j]);
                                                }
                                            }
                                            start = start.plusDays(1);
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 4: {
                                    try {
                                        int proid = l[0].getProductId();
                                        for (int i = 1; i < l.length; i++) {
                                            if (l[i - 1].getQuantity() <= l[i].getQuantity()) {
                                                proid = l[i].getProductId();
                                            }
                                        }
                                        for (int i = 0; i < d.length; i++) {
                                            if (d[i].getPro_id() == proid) {
                                                System.out.println(d[i]);
                                            }
                                            System.out.println(" ");
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 5: {
                                    try {
                                        int proid = l[0].getProductId();
                                        double profit = 1;
                                        for (int i = 1; i < l.length; i++) {
                                            if (l[i - 1].getQuantity() <= l[i].getQuantity()) {
                                                proid = l[i].getProductId();
                                                profit = l[i].getQuantity() * l[i].getPrice();
                                            }
                                        }
                                        for (int i = 0; i < d.length; i++) {
                                            if (d[i].getPro_id() == proid) {
                                                System.out.println("Product id :" + d[i].getPro_id());
                                                System.out.println("Product name :" + d[i].getProductName());
                                                System.out.println("profit amount =" + profit);
                                            }
                                        }
                                    } catch (NullPointerException npe) {
                                        System.out.println(npe);
                                    } catch (ArrayIndexOutOfBoundsException aiob) {
                                        System.out.println(aiob);
                                    }
                                    break;
                                }
                                case 6: {
                                    sts = false;
                                    break;
                                }
                                default: {
                                    System.err.println("Enter the correct menu option");
                                }
                            }
                        } while (sts);
                        break;
                    }
                    case 4: {
                        rw.customerDataWriter();
                        rw.productWriter();
                        rw.orderWriter();
                        rw.lineOrderWriter();
                        t = false;
                        break;
                    }
                    default: {
                        System.err.println("Enter the correct menu option");
                    }
                }
            } while (t);
        } catch (NullPointerException npe) {
            System.out.println(npe);
        } catch (ArrayIndexOutOfBoundsException aiob) {
            System.out.println(aiob);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public double addItems() {
        double total = 0;
        try {
            for (int j = 0; j < d.length; j++) {
                System.out.println(d[j]);
                System.out.println(" ");
            }
            System.out.println("enter order id");
            int orderId = Integer.parseInt(s1.readLine());

            double price = 1;
            boolean li = true;
            do {
                l = Arrays.copyOf(l, l.length + 1);
                System.out.println("enter product id");
                int productId = Integer.parseInt(s1.readLine());
                System.out.println("enter the quantity");
                int quantity = Integer.parseInt(s1.readLine());
                for (int i = 0; i < d.length; i++) {
                    if (d[i].getPro_id() == productId) {
                        int c = d[i].getProQuantity() - quantity;
                        price = d[i].getPrice() * quantity;
                        d[i].setProQuantity(c);
                        total += price;
                    }
                }
                l[l.length - 1] = new LineOrder(orderId, productId, quantity, price);
                System.out.println("1,add product");
                System.out.println("2,enough product");
                System.out.println("enter the option");
                int y = Integer.parseInt(s1.readLine());
                switch (y) {
                    case 1:
                        li = true;
                        break;
                    case 2:
                        li = false;
                        break;
                }
            } while (li);
            System.out.println("sucess");

        } catch (NullPointerException npe) {
            System.out.println(npe);
        } catch (ArrayIndexOutOfBoundsException aiob) {
            System.out.println(aiob);
        } catch (Exception e) {
            System.err.println(e);
        }
        return total;
    }
}
