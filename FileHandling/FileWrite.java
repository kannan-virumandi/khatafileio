/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FileHandling;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author bas200188
 */
public class FileWrite {

    public void customerDataWriter() {
        DataOutputStream dw = null;

        try {
            dw = new DataOutputStream(new FileOutputStream("/home/bas200188/Documents/Kfiles/customer.txt"));
            for (int i = 0; i < FileHandling.KhataFileIo.cus.length; i++) 
            {
                    dw.writeInt(FileHandling.KhataFileIo.cus[i].getCusId());
                    dw.writeUTF(FileHandling.KhataFileIo.cus[i].getCustomerName());
                    dw.writeLong(FileHandling.KhataFileIo.cus[i].getPhoneNumber());
               

                }
            
            System.out.println("Customer File Copied");
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");
        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
        } finally {
            try {
                if (Objects.nonNull(dw)) {
                    dw.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }
        }

    }

    public void productWriter() {
        DataOutputStream dw = null;

        try {
            dw = new DataOutputStream(new FileOutputStream("/home/bas200188/Documents/Kfiles/Inventory.txt"));
            for (int i = 0; i < FileHandling.KhataFileIo.d.length; i++) {
                    dw.writeInt(FileHandling.KhataFileIo.d[i].getPro_id());
                    dw.writeUTF(FileHandling.KhataFileIo.d[i].getProductName());
                    dw.writeInt(FileHandling.KhataFileIo.d[i].getProQuantity());
                    dw.writeDouble(FileHandling.KhataFileIo.d[i].getPrice());
    

                
            } System.out.println("Product File Copied");
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");
        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
        } finally {
            try {
                if (Objects.nonNull(dw)) {
                    dw.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }
    }

    public void orderWriter() {
        DataOutputStream dw = null;

        try {
            dw = new DataOutputStream(new FileOutputStream("/home/bas200188/Documents/Kfiles/Order.txt"));
            for (int i = 0; i < FileHandling.KhataFileIo.or.length; i++) {
                    dw.writeInt(FileHandling.KhataFileIo.or[i].getOrderId());
                    dw.writeInt(FileHandling.KhataFileIo.or[i].getCusId());
                    dw.writeUTF(FileHandling.KhataFileIo.or[i].getDate() + "");
                    dw.writeUTF(FileHandling.KhataFileIo.or[i].getDeliveredAddress());
                    dw.writeDouble(FileHandling.KhataFileIo.or[i].getTotalprice());
                    dw.writeDouble(FileHandling.KhataFileIo.or[i].getBalancePayment());
                
                

            } System.out.println("Order File Copied");
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");
        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
        } finally {
            try {
                if (Objects.nonNull(dw)) {
                    dw.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }

        }
    }

    public void lineOrderWriter() {
        DataOutputStream dw = null;

        try {
            dw = new DataOutputStream(new FileOutputStream("/home/bas200188/Documents/Kfiles/lineOrder.txt"));
            for (int i = 0; i < FileHandling.KhataFileIo.l.length; i++) {

          
                    dw.writeInt(FileHandling.KhataFileIo.l[i].getOrderId());
                    dw.writeInt(FileHandling.KhataFileIo.l[i].getProductId());
                    dw.writeInt(FileHandling.KhataFileIo.l[i].getQuantity());
                    dw.writeDouble(FileHandling.KhataFileIo.l[i].getPrice());

          
               

            } System.out.println("LineOrder Files Copied");
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException occurs");
        } catch (IOException ioe) {
            System.out.println("IOException occurs");
            ioe.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("null pointer exception");
        } finally {
            try {
                if (Objects.nonNull(dw)) {
                    dw.close();
                }
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer exception");
            }
        }

    }

}
