/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package FileHandling;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200188
 */
public class FileRead {
    public void customerDataReader()  {
        DataInputStream dr = null;
        try {
            dr = new DataInputStream(new FileInputStream("/home/bas200188/Documents/Kfiles/customer.txt"));
            while (true) {
                int CId = dr.readInt();
                String cname = dr.readUTF();
                Long phno = dr.readLong();
                FileHandling.KhataFileIo.cus = Arrays.copyOf(FileHandling.KhataFileIo.cus, FileHandling.KhataFileIo.cus.length + 1);
               FileHandling.KhataFileIo.cus[FileHandling.KhataFileIo.cus.length-1] = new Customer(CId,cname,phno);
                 
            }
    
        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("Read completed");
        } finally {
            try {
                dr.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");
               }
            }
    }
    public void InvemtoryReader()  {
        DataInputStream dr = null;
        try {
            dr = new DataInputStream(new FileInputStream("/home/bas200188/Documents/Kfiles/Inventory.txt"));
            while (true) {
                int pId = dr.readInt();
                String pname = dr.readUTF();
                int quantity = dr.readInt();
                double price=dr.readDouble();
                FileHandling.KhataFileIo.d = Arrays.copyOf(FileHandling.KhataFileIo.d, FileHandling.KhataFileIo.d.length + 1);
               FileHandling.KhataFileIo.d[FileHandling.KhataFileIo.d.length-1] = new Inventory(pId,pname,quantity,price);
                 
            }
    
        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("Read completed");
        } finally {
            try {
                dr.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");
               }
            }
    }
    public void orderReader()  {
        DataInputStream dr = null;
        try {
            dr = new DataInputStream(new FileInputStream("/home/bas200188/Documents/Kfiles/Order.txt"));
            while (true) {
                int oid = dr.readInt();
                int cusid = dr.readInt();
                LocalDate date=LocalDate.parse(dr.readUTF());
                String deladd=dr.readUTF();
                double totalprice=dr.readDouble();
                double  balprice=dr.readDouble();
                FileHandling.KhataFileIo.or = Arrays.copyOf(FileHandling.KhataFileIo.or, FileHandling.KhataFileIo.or.length + 1);
               FileHandling.KhataFileIo.or[FileHandling.KhataFileIo.or.length-1] = new Order(oid,cusid,date,deladd,totalprice,balprice);
                 
            }
    
        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("Read completed");
        } finally {
            try {
                dr.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");
               }
            }
    }
    public void lineOrderReader()  {
        DataInputStream dr = null;
        try {
            dr = new DataInputStream(new FileInputStream("/home/bas200188/Documents/Kfiles/lineOrder.txt"));
            while (true) {
                int oid = dr.readInt();
                int pid=dr.readInt();
                 int qut=dr.readInt();
                double price=dr.readDouble();
              
                FileHandling.KhataFileIo.l = Arrays.copyOf(FileHandling.KhataFileIo.l, FileHandling.KhataFileIo.l.length + 1);
               FileHandling.KhataFileIo.l[FileHandling.KhataFileIo.l.length-1] = new LineOrder(oid,pid,qut,price);
                 
            }
    
        }catch(FileNotFoundException ffe){
            System.out.println("file not found exception");
        } catch (IOException ex) {
            System.out.println("Read completed");
        } finally {
            try {
                dr.close();
            } catch (IOException ex) {
                System.out.println("IOexception occurs");
            } catch (NullPointerException npe) {
                System.out.println("null pointer Exception occurs");
               }
            }
    }
    
    
}
