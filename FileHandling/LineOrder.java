package FileHandling;



/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author bas200188
 */

public class LineOrder {
    private int orderId;
    private int productId;
    private int quantity;
    private double price;
//    {
//      l[0]=new LineItems(12,121.121,3);
//    }
    public LineOrder() {
        super();
    }
     @Override
    public String toString() {
        return "order:  "+orderId+"   "+"product id:  "+productId+"  "+"Quantity :  "+quantity+"  "+"price ="+price;
    }
    public LineOrder(int orderId, int productId, int quantity,double price) {
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quantity;
        this.price=price;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
